-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 07 Jun 2020 pada 07.31
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_skripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_absensi_student`
--

CREATE TABLE `tbl_absensi_student` (
  `CtrlNo` int(11) NOT NULL,
  `IDUser` int(8) NOT NULL,
  `NIM` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `Absensi` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `Tanggal` date DEFAULT NULL,
  `JamMasuk` time DEFAULT NULL,
  `JamKeluar` time DEFAULT NULL,
  `Absenter` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_absensi_student`
--

INSERT INTO `tbl_absensi_student` (`CtrlNo`, `IDUser`, `NIM`, `Absensi`, `Tanggal`, `JamMasuk`, `JamKeluar`, `Absenter`) VALUES
(1, 4, '105011610014', 'Absent', '2020-03-22', NULL, NULL, NULL),
(2, 26, '105011610030', 'Absent', '2020-04-01', NULL, NULL, NULL),
(3, 3, '105011610006', 'Absent', '2020-04-01', NULL, NULL, NULL),
(4, 4, '105011610014', 'Hadir', '2020-04-01', '10:00:00', '11:30:00', 'Blessilia'),
(5, 4, '105011610014', 'Hadir', '2020-04-02', '10:00:00', '11:30:00', NULL),
(6, 4, '105011610014', 'Hadir', '2020-04-03', '10:00:00', '11:30:00', NULL),
(7, 26, '105011610030', 'Hadir', '2020-03-26', '10:00:00', '11:30:00', 'Blessilia Wurangian'),
(8, 26, '105011610030', 'Hadir', '2020-04-02', '10:00:00', '10:30:00', 'Blessilia Wurangian');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_student`
--

CREATE TABLE `tbl_student` (
  `IDUser` int(8) NOT NULL,
  `NIM` varchar(30) DEFAULT NULL COMMENT 'Elecronic ID Number, Social Security No',
  `NoReg` varchar(30) NOT NULL,
  `NamaLengkap` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `Prodi` varchar(20) CHARACTER SET utf8 NOT NULL,
  `TahunMasuk` varchar(10) CHARACTER SET utf8 NOT NULL,
  `JenisKelamin` varchar(10) CHARACTER SET utf8 NOT NULL,
  `TglLahir` date NOT NULL,
  `TempatLahir` tinytext CHARACTER SET utf8 NOT NULL,
  `Agama` varchar(20) CHARACTER SET utf8 NOT NULL,
  `Alamat` text CHARACTER SET utf8,
  `NoHP` varchar(15) NOT NULL,
  `Email` mediumtext NOT NULL,
  `Photo` text,
  `Register` varchar(10) CHARACTER SET utf8 NOT NULL,
  `TanggalRegister` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_student`
--

INSERT INTO `tbl_student` (`IDUser`, `NIM`, `NoReg`, `NamaLengkap`, `Prodi`, `TahunMasuk`, `JenisKelamin`, `TglLahir`, `TempatLahir`, `Agama`, `Alamat`, `NoHP`, `Email`, `Photo`, `Register`, `TanggalRegister`) VALUES
(3, '105011610006', 's21610016', 'Mitchella Magdalena Polimpung', 'Sistem Informasi', '2016', 'Perempuan', '1998-06-25', 'Tomohon', 'Protestan', 'Tondano, Koya', '082347332855', 'mitchellapolimpung@gmail.com', 'user.png', '000001', '2020-03-22'),
(4, '105011610014', 's21610024', 'Riven Darien Lumangkun', 'Sistem Informasi', '2016', 'Laki-laki', '1999-11-05', 'Jakarta', 'Advent', 'Jakarta', '089689870264', 'rivenlumangkun7@gmail.com', 'user.png', '000001', '2020-03-24'),
(5, '105021610013', 's21610171', 'Juan William Daniel Polii', 'Teknik Informatika', '2016', 'Laki-laki', '2020-08-11', 'Tomohon', 'Protestan', 'Tomohon, Wailan Ling V', '081213332297', 'juanpolii22@gmail.com', 'user.png', '000001', '2020-03-23'),
(6, '105011610015', 's21610194', 'Rivaldo Hiskia Likumarico', 'Sistem Informasi', '2016', 'Laki-laki', '1998-11-19', 'Langowan', 'Protestan', 'Langowan, Minahasa', '082290445003', 'rivaldolie199@gmail.com', 'user.png', '000001', '2020-04-10'),
(10, '105021610048', '1055021610030', 'Audrey Hartmen Joy Lord Regi Siar', 'Teknik Informatika', '2016', 'Laki-laki', '2000-08-14', 'Manado', 'Protestan', 'Maumbi, Minahasa Utara', '082187948658', 'siaraudre01@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(11, '105021610048', 's21610493', 'Engglin Angela Theresia Humbas', 'Teknik Informatika', '2016', 'Perempuan', '1998-07-03', 'Bekasi', 'Advent', 'Azalea Hall', '082190294240', 'engglin03@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(12, '105011610037', 's21610534', 'Lewi Kailola', 'Sistem Informasi', '2016', 'Laki-laki', '1999-01-22', 'Sailale', 'Protestan', 'Perum Pemda Kalawat, Minahasa Utara', '081340272051', 's21610534@student,unklab.ac.id', 'user.png', 's21610062', '2020-04-10'),
(14, '105011610023', 's21610242', 'Eugene Edwell Henrialen Nangoy', 'Sistem Informasi', '2016', 'Laki-laki', '1999-03-09', 'Manado', 'Protestan', 'Jl. W. Z. Yohanes, Bumi Nyiur ling V, Kota Manado', '082191085163', 's21610242@student.unklab.ac.id', 'user.png', 's21610062', '2020-04-10'),
(15, '105011610013', 's21430035', 'Jad Heluku', 'Sistem Informasi', '2016', 'Laki-laki', '1995-06-15', 'Iga', 'Advent', 'Jl. Lorong Kubur Paal 4, Kota Manado', '085397910849', 'jadheluku15@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(16, '105011610025', 's21610321', 'Hizkia Denny Lumempouw', 'Sistem Informasi', '2016', 'Laki-laki', '1998-09-27', 'Kawangkoan', 'Advent', 'Tompaso 2, Minahasa', '081527414781', 'hizkiadennylumempouw@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(17, '105011610046', 's21610477', 'Jesica Jenet Miriam Mussu', 'Teknik Informatika', '2016', 'Perempuan', '1998-01-14', 'Bitung', 'Protestan', 'Bitung', '083191012297', 'jesicamussu7@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(18, '10501161026', 's21610304', 'Rivaldo Vreyke Samuel Kaunang', 'Sistem Informasi', '2016', 'Laki-laki', '1999-02-04', 'Tondano', 'Protestan', 'Tanggari, Minahasa Utara', '081342710372', 'valdoraveler@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(19, '105011610012', 's21610170', 'Ryfen Reinheartchrist Rarumangkay', 'Teknik Informatika', '2016', 'Laki-laki', '1998-12-09', 'Tomohon', 'Protestan', 'Rerer, Kombi, Minahasa', '08124193564', 'ryfenrr@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(21, '105011610043', 's21610585', 'Raymond Cristof Tumiwa', 'Sistem Informasi', '2016', 'Laki-laki', '1996-01-22', 'Balikpapan', 'Protestan', 'Jl Lestari, Parigi atas, Malalayang I, Kota Manado', '08992733949', 'tumiwa22@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(22, '105021610015', 's21610184', 'Frankly Smit Pangemanan', 'Teknik Informatika', '2016', 'Laki-laki', '1999-02-10', 'Bannada', 'Advent', 'Jl Awoga, Desa Bannada, Kecamatan Gemeh, Talaud', '082271125238', 'pangemananfrankly@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(23, '105011610003', 's21610011', 'Sintya Valenda Hamise', 'Sistem Informasi', '2016', 'Perempuan', '1999-05-19', 'Palu', 'Advent', 'Olobaru, Parigi Moutong, Sulawesi selatan', '085394728280', 'sintyahamise023@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(24, '105011610031', 's21610421', 'Valentshea Megaputri Doringin', 'Sistem Informasi', '2016', 'Perempuan', '1999-06-16', 'Modoinding', 'Advent', 'Jl Raya Bongkudai Baru, Bolaangmongondow Timur, Kecamatan Moat', '08980897777', 'vhayrin@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(25, '2147483647', 's21610009', 'Army Kidung Nirwana Katilik', 'Sistem Informasi', '2016', 'Laki-laki', '1999-01-17', 'Manado', 'Advent', 'Maras lingkungan 3, Kecamatan Bunaken, Kota Manado', '081398636223', 'kidungnirwana@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(26, '105011610030', 's21610420', 'Marlivye Makapedua', 'Sistem Informasi', '2016', 'Perempuan', '1999-03-17', 'Manado', 'Protestan', 'Maumbi, Minahasa Utara', '085342846004', 'livyeyesica@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(27, '105011610001', 's21610214', 'Geraldo Belenzky Muaja', 'Teknik Informatika', '2016', 'Laki-laki', '1997-10-21', 'Talaitad', 'Advent', 'Perum Rizky Mumbi Permai blok I/23, Minahasa Utara', '082347197982', 's21610214@student.unklab.ac.id', 'user.png', 's21610062', '2020-04-10'),
(28, '105021610042', 's21610387', 'Daniel Putra Yudha Lokollo', 'Teknik Informatika', '2016', 'Laki-laki', '1998-10-20', 'Bitung', 'Advent', 'Bitung, Sulawesi Utara', '089609554222', 'yudhadaniel16@gmail.com', 'user.png', 's21610062', '2020-04-10'),
(29, '105021610053', 's21610540', 'Frendio Sendouw', 'Teknik Informatika', '2016', 'Laki-laki', '1998-09-09', 'Tondano', 'Protestan', 'Papakelan, Tondano', '081242058422', 's21610540@student.unklab.ac.id', 'user.png', 's21610062', '2020-04-10'),
(30, '105021610028', 's21610264', 'Jeison Gery Mailaira', 'Teknik Informatika', '2016', 'Laki-laki', '1999-06-19', 'Manado', 'Protestan', 'Bitung, Sulawesi Utara', '081245765761', 's21610264@student.unklab.ac.id', 'user.png', 's21610062', '2020-04-10'),
(31, '105021610052', 's21610538', 'Erasia Putri Kloah', 'Teknik Informatika', '2016', 'Perempuan', '1999-03-04', 'Port Moresby, Papua Guinea', 'Advent', 'Airmadidi Atas, Minahasa Utara', '0895331232658', 'erasiaputri@gmail.com', 'user.png', 's21610017', '2020-04-10'),
(33, '105021610143', 's21610143', 'Garry Ronaldo Israel Mongi', 'Teknik Informatika', '2016', 'Laki-laki', '1998-07-11', 'Tomohon', 'Protestan', 'Kamasi Satu, Jl Wisma, Lingkungan III, Tomohon', '0813429225130', 's21610143@student.unklab.ac.id', 'user.png', 's21610062', '2020-04-10'),
(34, '105021610006', 's21610065', 'Nehemia Mikhael Musak', 'Teknik Informatika', '2016', 'Laki-laki', '1999-04-10', 'Koyawas', 'Protestan', 'Kakas, Minahasa', '089647218974', 'mikhaelmusak@gmail.com', 'user.png', 's21610062', '2020-04-11');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userid` int(8) NOT NULL,
  `IDUser` varchar(10) NOT NULL,
  `name` varchar(40) NOT NULL,
  `NIM` varchar(30) NOT NULL,
  `uname` varchar(15) NOT NULL,
  `password` varchar(65) NOT NULL,
  `level` varchar(30) NOT NULL,
  `date` date NOT NULL,
  `login` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`userid`, `IDUser`, `name`, `NIM`, `uname`, `password`, `level`, `date`, `login`) VALUES
(1, 's21610062', '', '105011610010', 'Blessilia', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Admin', '2020-03-22', 'Logout'),
(2, 's21610017', '', '105011610005', 'Bela', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Admin', '2020-03-22', 'Logout'),
(3, 's21610016', 'Mitchella Magdalena Polimpung', '105011610006', '105011610006', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(4, 's21610024', 'Riven Darien Lumangkun', '105011610014', '105011610014', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(5, 's21610171', 'Juan William Daniel Polii', '105021610013', '105021610013', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(6, 's21610194', 'Rivaldo Hiskia Likumarico', '105011610015', '105011610015', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(7, 's21610303', 'Audrey Hartmen Joy Lord Regi Siar', '1055021610030', '1055021610030', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(8, 's21610493', 'Engglin Angela Theresia Humbas', '105021610048', '105021610048', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(9, 's21610534', 'Lewi Kailola', '105011610037', '105011610037', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(11, 's21610242', 'Eugene Edwell Henrialen Nangoy', '105011610023', '105011610023', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(12, 's21430035', 'Jad Heluku', '105011610013', '105011610013', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(13, 's21610321', 'Hizkia Denny Lumempouw', '105011610025', '105011610025', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(14, 's21610477', 'Jesica Jenet Miriam Mussu', '105011610046', '105011610046', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(15, 's21610304', 'Rivaldo Vreyke Samuel Kaunang', '10501161026', '10501161026', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(16, 's21610170', 'Ryfen Reinheartchrist Rarumangkay', '105011610012', '105011610012', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(18, 's21610585', 'Raymond Cristof Tumiwa', '105011610043', '105011610043', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(19, 's21610184', 'Frankly Smit Pangemanan', '105021610015', '105021610015', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(20, 's21610011', 'Sintya Valenda Hamise', '105011610003', '105011610003', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(21, 's21610421', 'Valentshea Megaputri Doringin', '105011610031', '105011610031', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(22, 's21610009', 'Army Kidung Nirwana Katilik', '105011610001', '105011610001', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(24, 's21610214', 'Geraldo Belenzky Muaja', '105021610021', '105021610021', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(25, 's21610387', 'Daniel Putra Yudha Lokollo', '105021610042', '105021610042', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(26, 's21610540', 'Frendio Sendouw', '105021610053', '105021610053', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(27, 's21610264', 'Jeison Gery Mailaira', '105021610028', '105021610028', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(28, 's21610538', 'Erasia Putri Kloah', '105021610052', '105021610052', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(29, 's21610143', 'Garry Ronaldo Israel Mongi', '105021610143', '105021610143', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout'),
(30, 's21610065', 'Nehemia Mikhael Musak', '105021610006', '105021610006', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-07', 'Logout'),
(32802936, 's21610420', 'Marlivye Makapedua', '105011610030', '105011610030', '$2y$10$/mcIeton1TEMZWYJwJsOGuGo5ahTmjQvZfZfnKHyClVLVYu9FBKZ2', 'Student', '2020-04-10', 'Logout');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_absensi_student`
--
ALTER TABLE `tbl_absensi_student`
  ADD PRIMARY KEY (`CtrlNo`),
  ADD KEY `IDUser` (`IDUser`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`IDUser`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userid`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_absensi_student`
--
ALTER TABLE `tbl_absensi_student`
  MODIFY `CtrlNo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `IDUser` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userid` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32802937;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `tbl_absensi_student`
--
ALTER TABLE `tbl_absensi_student`
  ADD CONSTRAINT `tbl_absensi_student_ibfk_1` FOREIGN KEY (`IDUser`) REFERENCES `tbl_student` (`IDUser`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
