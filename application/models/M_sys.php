<?php
class M_sys extends CI_Model {
    public function __construct() {
        parent::__construct();
    } 

    public function select($query) {
        $sql = $this->db->query($query);
        return $sql->result();
    }
    // public function insert($table, $data) {
    //     $this->db->insert($table, $data);
    // }
    function insert($table, $data) {
        $data = $this->db->insert($table, $data);
        if ($data) {
            # code...
            return true;
        }else{ 
            return false;
        }
    }
    
    public function update($table, $data, $where) { 
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    
    public function delete($table, $where) {
        $this->db->where($where);
        $this->db->delete($table);
    }
    
    function get_cur_user(){
        $idn = $this->session->userdata('IDUser');
        $users = $this->load->database('users', true);
        $user = $users->query("SELECT IDUser, uname AS username FROM tbl_users where IDUser = '$idn'");
        if ($user->num_rows() > 0) {
            return $user->result();
        }else{
            return null;
        }
    }

    function get_autono(){
        $this->db->select('IDUser');
        $this->db->from('tbl_student');
        $this->db->limit(1);
        $this->db->order_by('IDUser','DESC');
        $query = $this->db->get();
        $querys = $this->db->affected_rows();
        if ($querys > 0) {
            # code...
            return $query->result();
        }else{
            return null;
        }
    }

    public function delete_student($id){
        $this->db->where('IDUser',$id);
        $this->db->delete('tbl_student');
    }


    function get_data_student(){
        $data = $this->db->query("SELECT * FROM tbl_student");
         if ($data) {
            return $data->result();
        }else{
            return null;
        }
    }

    function get_data_student_bynim($nim){
        $data = $this->db->query("SELECT * FROM tbl_student WHERE NIM = '$nim'");
         if ($data) {
            return $data->result();
        }else{
            return null; 
        }
    }

    function count_absent_bynim($nim){
        $data = $this->db->query("SELECT Tanggal, COUNT(Absensi) as total_abs FROM tbl_absensi_student WHERE NIM = '$nim' AND Absensi='Absent' ORDER BY Tanggal ASC");
         if ($data) {
            return $data->result();
        }else{
            return null;
        }
    }

    function count_hadir_bynim($nim){
        $data = $this->db->query("SELECT Tanggal, COUNT(Absensi) as total_hadir FROM tbl_absensi_student WHERE NIM = '$nim' AND Absensi='Hadir' ORDER BY Tanggal ASC");
         if ($data) {
            return $data->result();
        }else{
            return null;
        }
    }

    function select_date_absent_bynim($nim){
        $data = $this->db->query("SELECT * FROM tbl_absensi_student WHERE NIM = '$nim'");
         if ($data) {
            return $data->result();
        }else{
            return null;
        }
    }

     function get_data_sum_student(){
         $data = $this->db->query("SELECT sub.JLakilaki,sub.JPerempuan,sub.Total FROM (SELECT JenisKelamin,SUM(IF(tb1.JenisKelamin = 'Laki-laki',1,0)) AS JLakilaki, SUM(IF(tb1.JenisKelamin = 'Perempuan',1,0)) AS JPerempuan, COUNT(tb1.NIM) AS Total FROM(SELECT IDUser,NIM ,JenisKelamin FROM tbl_student)tb1)sub");
        if ($data) {
            return $data->result();
        }else{
            return null;
        }
    }

}