<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class M_login extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function select($query) {
        $sql = $this->db->query($query);
        return $sql->result();
    }
    
    public function insert($table, $data) {
        $this->db->insert($table, $data);
    }
    
    public function update($table, $data, $where) {
        $this->db->where($where);
        $this->db->update($table, $data);
    }
    
    public function delete($table, $where) {
        $this->db->where($where); 
        $this->db->delete($table);
    }

    function get_pass($uname){
        $users = $this->load->database('users', true);

        $pass = $users->query("SELECT * FROM tbl_users WHERE uname = '$uname'");
        if ($pass->num_rows() > 0) {
            return $pass->row();
        }else{
            return null;
        }
    }

    function update_status($status, $uname){
        $users = $this->load->database('users', true);
        $users->query("UPDATE tbl_users SET login = '$status' WHERE uname = '$uname'");
    }
}