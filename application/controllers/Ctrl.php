<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ctrl extends CI_Controller {
    public function __construct() {   
        parent::__construct();
        $this->load->model('M_sys');
        $this->load->library('session');
        if ($this->session->userdata('userlogin') != TRUE) {
            $this->session->set_flashdata('notif', '<font color="red"><center>Login First!</center></font>');
            redirect('Login/index');
        }
    }
    //Admin
    public function index(){ 
        $query['curuser'] = $this->M_sys->get_cur_user();
        $query['title'] = 'Dashboard';
        $query['header'] = 'Dashboard';
        $query['subheader'] = 'Dashboard';
        $query['sumstudent'] = $this->M_sys->get_data_sum_student();
        $query['data'] = $this->M_sys->get_data_student();
        $this->load->view('dashboard/v_dashboard',$query);
    }

    public function view_student_reg(){ 
        $query['curuser'] = $this->M_sys->get_cur_user();
        $query['title'] = 'Dashboard';
        $query['header'] = 'Dashboard';
        $query['subheader'] = 'Dashboard';
        $query['data'] = $this->M_sys->get_data_student();
        $this->load->view('student/v_student_reg',$query);
    }

    public function view_detail_data_student($nim){ 
        $query['curuser'] = $this->M_sys->get_cur_user();
        $query['title'] = 'Student Profile';
        $query['header'] = 'Dashboard';
        $query['subheader'] = 'Detail Data';
        $query['data'] = $this->M_sys->get_data_student_bynim($nim);
        $query['ttl_absent'] = $this->M_sys->count_absent_bynim($nim);
        $query['ttl_hadir'] = $this->M_sys->count_hadir_bynim($nim);
        $query['date_absent'] = $this->M_sys->select_date_absent_bynim($nim);
        $this->load->view('student/v_detail_data_student',$query);
    }
    public function new_data_student(){ 
        $query['curuser'] = $this->M_sys->get_cur_user();
        $query['title'] = 'New Data';
        $query['header'] = 'Registrasi Student';
        $query['subheader'] = 'Form Registration';
        // $query['autono'] = $this->M_sys->get_autono();
        // if ($query['autono'] == '') {
        //   $aut = 0;
        // }else{
        // $aut=$query['autono'][0]->IDUser;
        // }
        // $query['autonomor'] = $aut + 1;
        $this->load->view('student/v_new_data_student',$query);
    }

    function transfer_data(){
        if ($this->input->post('submit')) {
            $query['MNim'] = $nim_ = $this->input->post('NIM');
            $query['MNoReg'] = $noreg_ = $this->input->post('NoReg');
            $query['MNamaLengkap'] = $namalengkap_ = $this->input->post('NamaLengkap');
            $query['MJenisKelamin'] = $jeniskelamin_ = $this->input->post('JenisKelamin');
            $query['MTglLahir'] = $tgllahir_ = $this->input->post('TglLahir');
            $query['MTempatLahir'] = $tempatlahir_ = $this->input->post('TempatLahir');
            $query['MAgama'] = $agama_ = $this->input->post('Agama');
            $query['MKewarganegaraan'] = $kewarganegaraan_ = $this->input->post('Kewarganegaraan');
            $query['MSuku'] = $suku_ = $this->input->post('Suku');
            $query['MAlamat'] = $alamat_ = $this->input->post('Alamat');
            $query['MNoHP'] = $nohp_ = $this->input->post('NoHP');
            $query['MEmail'] = $email_ = $this->input->post('Email');

            if(isset($_FILES['photo']) && $_FILES['photo']['name'] != ''){
            // $this->ddoo_upload('photo');
            // $photo = $_FILES['photo']['name'];
            $photo = $this->upload_image('photo');
            }else{
                $photo = "";
            }
            $datainsert = array(
              'Nim' =>$nim_,
              'NoReg' =>$noreg_,
              'NamaLengkap' =>$namalengkap_,
              'JenisKelamin' =>$jeniskelamin_,
              'TglLahir' =>$tgllahir_,
              'TempatLahir' =>$tempatlahir_,
              'Agama' =>$agama_,
              'Kewarganegaraan' =>$kewarganegaraan_,
              'Suku' =>$suku_,
              'Alamat' =>$alamat_,
              'NoHP' =>$nohp_,
              'Email' =>$email_,
              'Photo' =>$photo,
              'Register' => $this->session->userdata('IDUser'),
              'TanggalRegister' => date('Y-m-d')
            );    
            $this->M_sys->insert('tbl_student', $datainsert);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissable"> <button type="button" data-dismiss="alert" aria-hidden="true" class="close">&times;</button><center><strong>Success! </strong>Data has been added</center> </div>');
             redirect('Ctrl/new_data_student');               
        }
    }

    function deletestudent($id){
        $this->M_sys->delete_student($id);
        $this->session->set_flashdata('success_delete', '<div class="alert alert-success alert-dismissable"> <button type="button" data-dismiss="alert" aria-hidden="true" class="close">&times;</button> <center><strong>Success!</strong> data has been delete</center> </div>');
        redirect('Ctrl/index');
        //redirect('HR_Modul/view_all_careeranddevelopment');
    }



    //Student
    public function index_student(){ 
        $nim = $query['unim'] = $this->session->userdata('NIM');
        $query['curuser'] = $this->M_sys->get_cur_user($nim);
        $query['title'] = 'Student Profile';
        $query['header'] = 'Student Profile';
        $query['subheader'] = 'Detail Data';
        $query['data'] = $this->M_sys->get_data_student_bynim($nim);
        $query['ttl_absent'] = $this->M_sys->count_absent_bynim($nim);
        $query['ttl_hadir'] = $this->M_sys->count_hadir_bynim($nim);
        $query['date_absent'] = $this->M_sys->select_date_absent_bynim($nim);
        $this->load->view('student/v_student_profile_only',$query);
    }

}

