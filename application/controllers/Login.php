<?php 
class Login extends CI_Controller {
    public function __construct() {   
        parent::__construct();
        $this->load->model('M_login');
    }
    function index(){
        $this->load->view('login/v_login');
    }
    function verify_user() {
        //membuat rules untuk proses validasi
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('userpassword', 'userpassword', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['login'] = ""; 
            $this->load->view('login/v_login', $data);   
        } else {
            $uname = $this->input->post('username');
            $this->session->set_userdata('users', $uname);
            $upassword = $this->input->post('userpassword');
            $cekUser['row'] = $this->M_login->get_pass($uname);
            if ($cekUser['row'] != null) {
                foreach ($cekUser as $row) {
                    $cek = $row->password;
                    $uid = $row->userid;
                    $unm = $row->uname;
                    $uidn = $row->IDUser;
                    $unim = $row->NIM;
                    $userlevel = $row->level;
                    $userlogin = $row->login;
                    $status = "Login";
                    $t = "true";
                    if (password_verify($upassword, $cek)) {
                        # code...
                        if ($userlogin == "Logout") {
                            # code...
                                $newdata=array(
                                'userid' => $uid,
                                'username' => $unm,
                                'IDUser' => $uidn,
                                'NIM' => $unim,
                                'level' => $userlevel,
                                'userlogin' => TRUE
                                );
                            $this->session->set_userdata($newdata);
                            $this->M_login->update_status($status, $unm);
                            if($this->session->userdata('level') == 'Admin'){
                                redirect('Ctrl/index');
                            }
                            else if($this->session->userdata('level') == 'Student'){
                                redirect('Ctrl/index_student');
                            }
                            else{
                                 $this->session->set_flashdata('notif', '<div class="alert alert-danger"><center><font color="red">Access denied, user level cannot be access the system</font></center></div>');
                                redirect('Login');
                            }
                        }else{
                            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><center><font color="red">Please ask <strong>administrator</strong> to reset your login</font></center></div>');
                            $this->session->set_flashdata('cek', $t); 
                            redirect('Login/index');
                        }
                    }else{
                        $this->session->set_flashdata('notif', '<div class="alert alert-danger"><center><font color="red"><strong>Password</strong> does not match, please input valid password.</font></center></div>');
                        redirect('Login/index');
                    }
                }
            }else{
                $this->session->set_flashdata('notif', '<div class="alert alert-danger"><center><font color="red">Wrong <strong>username</strong> or <strong>password</strong>, please input valid one</font></center></div>');
                redirect('Login/index');
            }
        }
    }
    function logout() {
        //update status dari user yang sedang login menjadi logout
        $status = "Logout";
        $uname =  $this->session->userdata('username');
        $this->M_login->update_status($status, $uname);
        $this->session->sess_destroy();
        $data_login = array(
            'IDUser' => $this->session->userdata('IDUser'),
            'Status' => 'Logout'
        );
        redirect('Login');
    }
}