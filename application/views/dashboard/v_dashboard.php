<?php $this->load->view('fileheader_footer/header'); ?>
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $header; ?></div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active"><?php echo $subheader; ?></li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
    <div class="page-content">
        <?php echo $this->session->flashdata('success_delete'); ?>
        <div id="tab-general">
            <div id="sum_box" class="row mbl">
            <?php if ($sumstudent != null){ ?>
                <?php foreach ($sumstudent as $scum){ ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel income db mbm">
                            <div class="panel-body"><p class="icon"><i class="icon fa fa-male"></i></p><h4 class="value"><span><?php echo $scum->JLakilaki ?></span></h4>

                                <p class="description">Laki-laki</p>

                                <div class="progress progress-sm mbn">
                                    <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-info"><span class="sr-only">100% Complete (success)</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel task db mbm">
                            <div class="panel-body"><p class="icon"><i class="icon fa fa-female"></i></p><h4 class="value"><span><?php echo $scum->JPerempuan ?></span></h4>

                                <p class="description">Perempuan</p>

                                <div class="progress progress-sm mbn">
                                    <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-danger"><span class="sr-only">100% Complete (success)</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel visit db mbm">
                            <div class="panel-body"><p class="icon"><i class="icon fa fa-users"></i></p><h4 class="value"><span><?php echo $scum->Total ?></span></h4>

                                <p class="description">Total Student</p>

                                <div class="progress progress-sm mbn">
                                    <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-warning"><span class="sr-only">100% Complete (success)</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php }else{ ?>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel income db mbm">
                            <div class="panel-body"><p class="icon"><i class="icon fa fa-male"></i></p><h4 class="value"><span>0</span></h4>

                                <p class="description">Laki-laki</p>

                                <div class="progress progress-sm mbn">
                                    <div role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-info"><span class="sr-only">100% Complete (success)</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel task db mbm">
                            <div class="panel-body"><p class="icon"><i class="icon fa fa-female"></i></p><h4 class="value"><span>0</span></h4>

                                <p class="description">Perempuan</p>

                                <div class="progress progress-sm mbn">
                                    <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-danger"><span class="sr-only">100% Complete (success)</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="panel visit db mbm">
                            <div class="panel-body"><p class="icon"><i class="icon fa fa-users"></i></p><h4 class="value"><span>0</span></h4>

                                <p class="description">Total Student</p>

                                <div class="progress progress-sm mbn">
                                    <div role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-warning"><span class="sr-only">100% Complete (success)</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    	<div class="panel panel-blue">
            <div class="panel-heading">Daftar Student</div>
            <div class="panel-body">
                <table class="table table-hover table-bordered table-stripped">
                    <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th>Nama Lengkap</th>
                        <th>Jens Kelamin</th>
                        <th>Tgl Lahir</th>
                        <th>Tempat Lahir</th>
                        <th>Agama</th>
                        <th>No. HP</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($data != ""){ ?>
	                    <?php $no=1; foreach ($data as $dt){ ?>		
		                    <tr>
		                        <td width="5%" align="center" ><?php echo $no; ?></td>
		                        <td width="20%"><?php echo $dt->NamaLengkap ?></td>
		                        <td width="10%"><?php echo $dt->JenisKelamin ?></td>
		                        <td width="10%"><?php echo date('d-M-Y', strtotime($dt->TglLahir)); ?></td>
		                        <td width="20%"><?php echo $dt->TempatLahir ?></td>
		                        <td width="15%"><?php echo $dt->Agama ?></td>
                                <td width="20%"><?php echo $dt->NoHP ?></td>
		                    </tr>
	                    <?php $no++;} ?>
                    <?php }else{ ?>
                    	<tr>
                    		<td><center><font color="red">Tidak ada data!</font></center></td>
                    	</tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <!--END CONTENT--><!--BEGIN FOOTER-->
    <div id="footer">
        <div class="copyright">2020 © Admin - Web System</div>
    </div>
</div>
<?php $this->load->view('fileheader_footer/footer'); ?>