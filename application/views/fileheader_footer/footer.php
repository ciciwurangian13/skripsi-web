</div>
<script src="<?php echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<!--loading bootstrap js-->
<script src="<?php echo base_url(); ?>assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js"></script>
<script src="<?php echo base_url(); ?>assets/js/html5shiv.js"></script>
<script src="<?php echo base_url(); ?>assets/js/respond.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/slimScroll/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/iCheck/custom.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jquery-news-ticker/jquery.news-ticker.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.menu.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/jquery-pace/pace.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/holder/holder.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/responsive-tabs/responsive-tabs.js"></script>
<!--LOADING SCRIPTS FOR PAGE-->
<script src="<?php echo base_url(); ?>assets/vendors/intro.js/intro.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.categories.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.tooltip.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.fillbetween.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.stack.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/flot-chart/jquery.flot.spline.js"></script>
<script src="<?php echo base_url(); ?>assets/vendors/calendar/zabuto_calendar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/index.js"></script>
<!--CORE JAVASCRIPT-->
<script src="<?php echo base_url(); ?>assets/js/main.js"></script>
<script>(function (i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function () {
        (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
ga('create', 'UA-145464-12', 'auto');
ga('send', 'pageview');


</script>
</body>
</html>