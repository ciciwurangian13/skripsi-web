<?php $this->load->view('fileheader_footer/header'); ?>
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $header; ?></div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active"><?php echo $subheader; ?></li>
        </ol>
        <div class="clearfix"></div>
    </div>

    <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
    <div class="page-content">
        <?php echo $this->session->flashdata('success_edit'); ?>
        <div class="panel panel-blue">
            <div class="panel-heading">Daftar Student Register<a href="<?php echo site_url('Ctrl/new_data_student') ?>" type="button" class="btn btn-warning btn-xs pull-right"><i class="fa fa-plus"></i>&nbsp;New</a></div>
            <div class="panel-body">
                <table class="table table-hover table-bordered table-stripped">
                    <thead>
                    <tr>
                        <th class="text-center">No</th>
                        <th>Name Lengkap</th>
                        <th>Jenis Kelamin</th>
                        <th>Tgl Lahir</th>
                        <th>Tempat Lahir</th>
                        <th>Agama</th>
                        <th class="text-center"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if ($data != ""){ ?>
                        <?php $no=1; foreach ($data as $dt){ ?>     
                            <tr>
                                <td width="5%" align="center" ><?php echo $no; ?></td>
                                <td width="30%"><?php echo $dt->NamaLengkap ?></td>
                                <td width="10%"><?php echo $dt->JenisKelamin ?></td>
                                <td width="10%"><?php echo date('d-M-Y', strtotime($dt->TglLahir)); ?></td>
                                <td width="20%"><?php echo $dt->TempatLahir ?></td>
                                <td width="10%"><?php echo $dt->Agama ?></td>
                                <td width="10%">
                                    <center>
                                        <a href="<?php echo site_url('Ctrl/view_detail_data_student/'.$dt->NIM); ?>" type="button" class="btn btn-success btn-xs"><i class="fa fa-pencil"></i>
                                        </a>
                                        <a href="<?php echo site_url('Ctrl/deletestudent/'.$dt->IDUser); ?>" type="button" class="btn btn-danger btn-xs" onclick="return confirm('Apakah anda yakin akan menghapus data ini?')"><i class="fa fa-trash-o"></i>
                                        </a>
                                    </center>
                                </td>
                            </tr>
                        <?php $no++;} ?>
                    <?php }else{ ?>
                        <tr>
                            <td><center><font color="red">Tidak ada data!</font></center></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div> 
    </div>
    <!--END CONTENT--><!--BEGIN FOOTER-->
    <div id="footer">
        <div class="copyright">2020 © Admin - Web System</div>
    </div>
</div>
<?php $this->load->view('fileheader_footer/footer'); ?>