<?php $this->load->view('fileheader_footer/header_student_profile'); ?>
<style>
    .zoom {
        transition: transform .2s;
        /* Animation */
        margin: 0 auto;
    }

    .zoom:hover {
        transform: scale(1.5);
        /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
    }

@media (min-width: 992px){
    .page-wrapper .page-content {
        margin-left: -10px;        
    }

    .jumptarget::before {
      content:"";
      display:block;
      height:20px; /* fixed header height*/
      margin:-50px 0 0; /* negative fixed header height */
    }
}
</style>
<div id="page-wrappers"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
       
        
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
    <div class="page-content">
       <div class="row">
        <?php if ($data != ""){ ?>
            <?php foreach ($data as $dt){ ?>     
                <div class="col-md-12" style="margin-top: -20px"><h2>Profile : <b><?php echo $dt->NamaLengkap ?></b></h2>
                    <div class="row mtl">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="text-center mbl"><img src="<?php echo base_url(); ?>uploadfile/<?php echo $dt->Photo ?>"  width="100%" height="100%" alt="" class="img-responsive"/></div>
                            </div>
                            <table class="table">
                                <tbody>
                                    <tr style="background-color: white">
                                        <td>Nomor Register </td>
                                        <td><b><?php echo $dt->NoReg ?></b></td>
                                    </tr>
                                    <tr style="background-color: white">
                                        <td>NIM</td>
                                        <td><b><?php echo $dt->NIM ?></b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-9">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-detail" data-toggle="tab">Detail Profile</a></li>
                                <li><a href="#tab-absent" data-toggle="tab">Detail Absent</a></li>
                            </ul>
                            <div id="generalTabContent" class="tab-content">
                                <div id="tab-detail" class="tab-pane fade in active">
                                    <form action="#" class="form-horizontal" style="margin-top: -20px">
                                        <h3><b>Profile Detail</b></h3>
                                        <div class="form-group"><label class="col-sm-3 control-label">Nama Lengkap</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-9"><input value="<?php echo $dt->NamaLengkap ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-3 control-label">Prodi</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-9"><input value="<?php echo $dt->Prodi ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-3 control-label">Tahun Masuk</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-4"><input value="<?php echo $dt->TahunMasuk   ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group"><label class="col-sm-3 control-label">Jenis Kelamin</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-4"><input value="<?php echo $dt->JenisKelamin ?>" readonly  class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Tanggal Lahir</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-4"><input  value="<?php echo date('d-M-Y', strtotime($dt->TglLahir)); ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Tempat Lahir</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-4"><input  value="<?php echo $dt->TempatLahir ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Agama</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-4"><input value="<?php echo $dt->Agama ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                       
                                        <hr/>
                                        <h3><b>Contact Info</b></h3>

                                        <div class="form-group"><label class="col-sm-3 control-label">No. HP</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-9"><input value="<?php echo $dt->NoHP ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Email</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-9"><input value="<?php echo $dt->Email ?>" readonly  class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-3 control-label">Alamat</label>

                                            <div class="col-sm-9 controls">
                                                <div class="row">
                                                    <div class="col-xs-9"><input value="<?php echo $dt->Alamat ?>" readonly class="form-control"/></div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="tab-absent" class="tab-pane fade in" style="background-color: #f0f2f5">
                                    <div id="sum_box" class="row mbl" >
                                        <div class="col-md-4">
                                            <div class="panel profit db mbm">
                                                <div class="panel-body">
                                                    <p class="icon"><i class="icon fa fa-check"></i></p>
                                                    <h4 class="value">
                                                        <span>
                                                            <font color="green" size="8">
                                                                <b>
                                                                    <?php foreach ($ttl_hadir as $ttlhadir){ ?>
                                                                        <?php echo $ttlhadir->total_hadir ?>
                                                                    <?php } ?>
                                                                </b>
                                                            </font>
                                                        </span>
                                                    </h4>
                                                    <p class="description"><h3><b>Hadir</b></h3></p>
                                                    <div class="progress progress-sm mbn">
                                                        <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-success"><span class="sr-only">100% Complete (success)</span></div>
                                                    </div>
                                                </div>
                                                <div class="panel-body">
                                                    <p class="icon"><i class="icon fa fa-times"></i></p>
                                                    <h4 class="value">
                                                        <span>
                                                            <font color="red" size="8">
                                                                <b>
                                                                    <?php foreach ($ttl_absent as $ttl){ ?>
                                                                        <?php echo $ttl->total_abs ?>
                                                                    <?php } ?>
                                                                </b>
                                                            </font>
                                                        </span>
                                                    </h4>
                                                    <p class="description"><h3><b>Absent</b></h3></p>
                                                    <div class="progress progress-sm mbn">
                                                        <div role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;" class="progress-bar progress-bar-danger"><span class="sr-only">100% Complete (success)</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="panel panel-blue">
                                                <div class="panel-heading">Detail Tanggal Absent</div>
                                                <div class="panel-body">
                                                    <table class="table table-hover table-bordered table-stripped">
                                                        <thead>
                                                        <tr>
                                                            <th class="text-center">No</th>
                                                            <th class="text-center">Tanggal</th> 
                                                            <th class="text-center">Status Absensi</th> 
                                                            <th class="text-center">Jam Masuk</th>
                                                            <th class="text-center">Jam Keluar</th>
                                                            <th class="text-center">Absenter</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if ($date_absent != null){ ?>
                                                                <?php $no=1; foreach ($date_absent as $dateabs){ ?>
                                                                    <tr>
                                                                        <td width="10%" align="center" ><b><?php echo $no; ?></b></td>
                                                                        <td width="20%" align="center"><b><?php echo date('d-M-Y', strtotime($dateabs->Tanggal)); ?></b></td>
                                                                        <?php if ($dateabs->Absensi == 'Hadir'){ ?>
                                                                            <td width="30%" align="center" style="background-color: green" ><font color="white"><?php echo $dateabs->Absensi ?></font></td>
                                                                        <?php }else{ ?>
                                                                            <td width="30%" align="center" style="background-color: red"><font color="white"><?php echo $dateabs->Absensi ?></font></td>
                                                                        <?php } ?>
                                                                        <td width="20%" align="center"><b><font color="green"><?php echo $dateabs->JamMasuk ?></font></b></td>
                                                                        <td width="20%" align="center"><b><font color="darkorange"><?php echo $dateabs->JamKeluar ?></font></b></td>
                                                                        <td width="20%" align="center"><b><font color="brow"><?php echo $dateabs->Absenter ?></font></b></td>

                                                                    </tr>
                                                                <?php $no++;} ?>
                                                            <?php }else{ ?>
                                                                <tr>
                                                                    <td colspan="10"><center><font color="red">Tidak ada data!</font></center></td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php }else{ ?>
            <tr>
                <td><center><font color="red">Tidak ada data!</font></center></td>
            </tr>
        <?php } ?>
        </div>
    </div>
    <!--END CONTENT--><!--BEGIN FOOTER-->
    <div id="footer">
        <div class="copyright">2020 © Admin - Web System</div>
    </div>
</div>
<?php $this->load->view('fileheader_footer/footer'); ?>