<?php $this->load->view('fileheader_footer/header'); ?>
<div id="page-wrapper"><!--BEGIN TITLE & BREADCRUMB PAGE-->
    <div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
        <div class="page-header pull-left">
            <div class="page-title"><?php echo $header; ?></div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a href="index.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;</li>
            <li class="active"><?php echo $subheader; ?></li>
        </ol>
        <div class="clearfix"></div>
    </div>
    <!--END TITLE & BREADCRUMB PAGE--><!--BEGIN CONTENT-->
    <div class="page-content">
    	<div id="form-layouts" class="row">
            <div class="row">
                <div class="col-lg-2">
                </div>
                <div class="col-lg-8">
                <?php echo $this->session->flashdata('notif'); ?>
                    <div class="panel panel-blue">
                        <div class="panel-heading">Form input registration</div>
                        <div class="panel-body pan">
                            <form method="post"action="<?php echo base_url(); ?>index.php/Ctrl/transfer_data" enctype="multipart/form-data">  
                                <div class="form-body pal" style="margin-top: -20px"><h3><i class="fa fa-male"></i> Personal</h3>
                                    <div class="row"> 
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputNomorRegistrasi" class="control-label">Nomor Registrasi <span class='require'>*</span></label>
                                                <div class="input-icon right"><input id="inputNomorRegistrasi" name="NoReg" type="text" placeholder="Enter number" required class="form-control"/></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputNIM" class="control-label">NIM <span class='require'>*</span></label>

                                                <div class="input-icon right"><input id="inputNIM" name="NIM" type="number" placeholder="Enter number" required class="form-control"/></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputLengkap" class="control-label">Nama Lengkap <span class='require'>*</span></label>

                                                <div class="input-icon right"><input id="inputLengkap" name="NamaLengkap" type="text" placeholder="Enter text" required class="form-control"/></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputAgama" class="control-label">Prodi <span class='require'>*</span></label>
                                                <select id="inputProdi" name="Prodi" required class="form-control">
                                                    <option value="SI">Sistem Informasi</option>
                                                    <option value="TI">Teknik Informatika</option>
                                                </select>
                                            </div>
                                        </div>

                                         <div class="col-md-6">
                                            <div class="form-group"><label for="inputLengkap" class="control-label">Tahun Masuk <span class='require'>*</span></label>

                                                <div class="input-icon right"><input id="inputLengkap" name="TahunMasuk" type="text" placeholder="Enter text" required class="form-control"/></div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group"><label for="selGender" class="control-label">Jenis Kelamin <span class='require'>*</span></label>
                                                <select id="selGender" name="JenisKelamin" required class="form-control">
                                                    <option value="Laki-laki">Laki-laki</option>
                                                    <option value="Perempuan">Perempuan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputTglLahir" class="control-label">Tgl Lahir <span class='require'>*</span></label><input id="inputTglLahir" type="date" name="TglLahir" required class="form-control"/></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputTempatLahir" class="control-label">Tempat Lahir <span class='require'>*</span></label>

                                                <div class="input-icon right"><input id="inputTempatLahir" name="TempatLahir" type="text" placeholder="Enter text" required class="form-control"/></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputAgama" class="control-label">Agama <span class='require'>*</span></label>
                                                <select id="inputAgama" name="Agama" required class="form-control">
                                                    <option value="Advent">Kristen (Advent)</option>
                                                    <option value="Protestan">Krister (Protestan)</option>
                                                    <option value="Islam">Islam</option>
                                                    <option value="Katolik">Katolik</option>
                                                    <option value="Hindu">Hindu</option>
                                                    <option value="Budha">Budha</option>
                                                    <option value="Khonghucu">Khonghucu</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputKewarganegaraan" class="control-label">Kewarganegaraan <span class='require'>*</span></label><input id="inputKewarganegaraan" name="Kewarganegaraan" type="text" placeholder="" required class="form-control"/></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputSuku" class="control-label">Suku <span class='require'>*</span></label><input id="inputSuku" name="Suku" type="text" placeholder="Enter text" required class="form-control"/></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputNoHp" class="control-label">Nomor Hp <span class='require'>*</span></label><input id="inputNoHp" name="NoHP" type="number" placeholder="Enter number" required class="form-control"/></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"><label for="inputEmail" class="control-label">Email <span class='require'>*</span></label>

                                                <div class="input-icon"><i class="fa fa-envelope"></i><input type="text" name="Email" placeholder="Enter text" required class="form-control"/></div>
                                            </div>
                                        </div>
                                    </div>
                                    <h3><i class="fa fa-pencil"></i> Alamat</h3>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group"><label for="inputAlamat" class="control-label">Alamat lengkap <span class='require'>*</span></label><input id="inputAlamat" name="Alamat" type="text" placeholder="" required="" class="form-control"/></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right pal">
                                    <input type="submit" name="submit" value="Submit" class="btn btn-primary" onclick="return confirm('Apakah anda yakin?')">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
        </div>             
    </div>
    <!--END CONTENT--><!--BEGIN FOOTER-->
    <div id="footer">
        <div class="copyright">2020 © Admin - Web System</div>
    </div>
</div>
<?php $this->load->view('fileheader_footer/footer'); ?>